import React, { Component } from 'react';

import ShowList from '../containers/show-list';
import ShowSchedule from '../containers/show-schedule';

class App extends Component {    
    render() {
        return (
            <div>
                <div className='row'>
                    <h3 className='col-sm website-title'>When Is My Show?</h3>
                    <div className='col-lg menu-search'>
                        <div className='search-results'>
                            <p>

                            </p>

                        </div>

                    </div>
                </div>
                <div className='row'>
                    <ShowSchedule className='col-sm'/>
                    <ShowList className='col-lg'/>
                </div>
            </div>
        );
    }
}

export default App;
