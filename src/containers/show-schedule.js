import React, { Component } from 'react';
import { connect } from 'react-redux';

var showsToday = [];
var showsTomorrow = [];
var showsInTwoDays = [];

class ShowSchedule extends Component {
    createList(){
        return this.props.shows.map((show) => {
            const time = Date.parse(show.last_episode_aires) - Date.parse(new Date());
            const daysTillShow = Math.floor(time/(1000*60*60*24));
            
            if(daysTillShow === -1){
                showsToday.push({title: show.name, id: show.showID});
            }
            if(daysTillShow === 0){
                showsTomorrow.push({title: show.name, id: show.showID});
            }
            if(daysTillShow === 1){
                showsInTwoDays.push({title: show.name, id: show.showID});
            }
            
            return 0;
        });
    }
    renderTodayList(){
        if(showsToday.length === 0){
                return <li className='list-group-item'>No show.</li>
        }
        return showsToday.map((show) => {
            return(
                <li key={show.id} className='list-group-item'>
                    {show.title}
                </li>
            );
        });
    }
    renderTomorrowList(){
        if(showsTomorrow.length === 0){
                return <li className='list-group-item'>No show.</li>
        }
        return showsTomorrow.map((show) => {
            return(
                <li key={show.id} className='list-group-item'>
                    {show.title}
                </li>
            );
        });
    }
    renderInTwoDaysList(){
        if(showsInTwoDays.length === 0){
                return <li className='list-group-item'>No show.</li>
        }
        return showsInTwoDays.map((show) => {
            return(
                <li key={show.id} className='list-group-item'>
                    {show.title}
                </li>
            );
        });
    }
    
    render(){
        this.createList();
        return(
            <div className='show-schedule'>
                <div>
                    <ul className='list-group list-group-flush'>
                        <li className='list-group-item disabled text-center'>Today</li>
                        {this.renderTodayList()}
                    </ul>
                </div>
                <div>
                    <ul className='list-group list-group-flush'>
                        <li className='list-group-item disabled text-center'>Tomorrow</li>
                        {this.renderTomorrowList()}
                    </ul>
                </div>
                <div>
                    <ul className='list-group list-group-flush'>
                        <li className='list-group-item disabled text-center'>In 2 days</li>
                        {this.renderInTwoDaysList()}
                    </ul>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return{
        shows: state.shows
    };
}

export default connect(mapStateToProps)(ShowSchedule);