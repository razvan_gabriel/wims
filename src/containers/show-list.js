import React, { Component } from 'react';
import { connect } from 'react-redux';

class ShowList extends Component {
    renderList() {
        return this.props.shows.map((show) => {
            return(
                    <div key={show.showID} className='card tv-show-card text-truncate'>
                        <img className='card-img-top' src={show.image_url} alt={show.name} />
                        <div className='card-body'>
                            <p className='card-text'><b>{show.name}</b></p>
                        </div>
                    </div>
            );
        });
    }
    
    render(){
        return(
            <div className='tv-shows-list'>
                {this.renderList()}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return{
        shows: state.shows
    };
}

export default connect(mapStateToProps)(ShowList);