export default function() {
    return [
        {	
            showID: 62560,
            name: "Mr. Robot",  
            last_episode_aires: "2018-01-11", 
            image_url: "https://image.tmdb.org/t/p/w300/qE0t9rlClIReax0d5tr3j300wUt.jpg"
        },
        {	
            showID: 624324560,
            name: "Mr. Robot",  
            last_episode_aires: "2017-12-19", 
            image_url: "https://image.tmdb.org/t/p/w300/qE0t9rlClIReax0d5tr3j300wUt.jpg"
        },
        {	
            showID: 622367560,
            name: "Mr. Robot",  
            last_episode_aires: "2017-12-19", 
            image_url: "https://image.tmdb.org/t/p/w300/qE0t9rlClIReax0d5tr3j300wUt.jpg"
        },
        {	
            showID: 628534560,
            name: "Mr. Robot",  
            last_episode_aires: "2017-12-19", 
            image_url: "https://image.tmdb.org/t/p/w300/qE0t9rlClIReax0d5tr3j300wUt.jpg"
        },
        {	
            showID: 62598760,
            name: "Mr. Robot",  
            last_episode_aires: "2017-12-19", 
            image_url: "https://image.tmdb.org/t/p/w300/qE0t9rlClIReax0d5tr3j300wUt.jpg"
        },
        {	
            showID: 625456860,
            name: "Mr. Robot",  
            last_episode_aires: "2017-12-19", 
            image_url: "https://image.tmdb.org/t/p/w300/qE0t9rlClIReax0d5tr3j300wUt.jpg"
        },
        {
            showID: 1418,
            name: "The Big Bang Theory",
            last_episode_aires: "2018-01-09",
            image_url: "https://image.tmdb.org/t/p/w300/ooBGRQBdbGzBxAVfExiO8r7kloA.jpg"
        }
    ];
}