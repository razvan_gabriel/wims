import { combineReducers } from 'redux';
//here we import the reducers
import ShowsReducer from './reducer_shows';

const rootReducer = combineReducers({
    shows: ShowsReducer
});

export default rootReducer;